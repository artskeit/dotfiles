#!/usr/bin/env bash

# Inspiration taken from https://macos-defaults.com

printf "${INFO}› Setting defaults inside MacOS${COLOR_RESET}\n"

#######################
### DOCK ###
#######################

# Put the Dock on the bottom of the screen
defaults write com.apple.dock "orientation" -string "bottom"
# Dock icon size of 36 pixels.
defaults write com.apple.dock "tilesize" -int "36"
# Autohide the Dock when mouse is out
defaults write com.apple.dock "autohide" -bool "true"
# Do not display recent apps in the Dock
defaults write com.apple.dock "show-recents" -bool "false"
# Only show active apps
defaults write com.apple.dock "static-only" -bool "true"

#######################
### SCREENSHOTS ###
#######################

# Remove the default shadow from screenshots
defaults write com.apple.screencapture "disable-shadow" -bool "true"
# Set screenshot location to `~/Downloads/Screenshots`
defaults write com.apple.screencapture "location" -string "~/Downloads/Screenshots"

#######################
### SAFARI ###
#######################

# Show full website URL
defaults write com.apple.Safari "ShowFullURLInSmartSearchField" -bool "true"
# Enable Developer
defaults write com.apple.Safari.SandboxBroker ShowDevelopMenu -bool true
defaults write com.apple.Safari.plist IncludeDevelopMenu -bool true
defaults write com.apple.Safari.plist WebKitDeveloperExtrasEnabledPreferenceKey -bool true
defaults write com.apple.Safari.plist "com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled" -bool true
defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

#######################
### FINDER ###
#######################

# Show all file extensions inside the Finder
defaults write NSGlobalDomain "AppleShowAllExtensions" -bool "true"
# Show hidden files inside the Finder
defaults write com.apple.finder "AppleShowAllFiles" -bool "true"
# Show path bar
defaults write com.apple.finder "ShowPathbar" -bool "true"
# Column view
defaults write com.apple.finder "FXPreferredViewStyle" -string "clmv"
# Keep folders on top
defaults write com.apple.finder "_FXSortFoldersFirst" -bool "true"
# Search the current folder
defaults write com.apple.finder "FXDefaultSearchScope" -string "SCcf"
# Do not display the warning when changing file extension
defaults write com.apple.finder "FXEnableExtensionChangeWarning" -bool "false"
# Home directory is opened in the fileviewer dialog when saving a new document
defaults write NSGlobalDomain "NSDocumentSaveNewDocumentsToCloud" -bool "false"
# Show icon in the title bar
defaults write com.apple.universalaccess "showWindowTitlebarIcons" -bool "true"
# Show the ~/Library folder.
chflags nohidden ~/Library

#######################
### TRACKPAD & KEYBOARD ###
#######################

# Enable dragging with drag lock
defaults write com.apple.AppleMultitouchTrackpad "DragLock" -bool "true"
# Set a really fast key repeat.
defaults write NSGlobalDomain KeyRepeat -int 1

#######################
### MISSION CONTROL ###
#######################

# Group windows by application.
defaults write com.apple.dock "expose-group-apps" -bool "true"
# Spaces span all displays.
defaults write com.apple.spaces "spans-displays" -bool "false"

#######################
### TEXTEDIT ###
#######################

# Rich text is disabled.
defaults write com.apple.TextEdit "RichText" -bool "false"
# Quotes will remain in the form they are typed.
defaults write com.apple.TextEdit "SmartQuotes" -bool "false"


printf "${SUCCESS}› Done. Note that some of these changes require a logout/restart to take effect.${COLOR_RESET}\n"
