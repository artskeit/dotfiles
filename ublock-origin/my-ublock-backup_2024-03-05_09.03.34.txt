{
  "timeStamp": 1709625814506,
  "version": "1.56.0",
  "userSettings": {
    "advancedUserEnabled": true,
    "importedLists": [],
    "popupPanelSections": 31
  },
  "selectedFilterLists": [
    "user-filters",
    "ublock-filters",
    "ublock-badware",
    "ublock-privacy",
    "ublock-quick-fixes",
    "ublock-unbreak",
    "easylist",
    "easyprivacy",
    "urlhaus-1",
    "plowe-0",
    "ublock-annoyances",
    "NLD-0",
    "SWE-1"
  ],
  "hiddenSettings": {},
  "whitelist": [
    "about-scheme",
    "chrome-extension-scheme",
    "chrome-scheme",
    "edge-scheme",
    "moz-extension-scheme",
    "opera-scheme",
    "vivaldi-scheme",
    "wyciwyg-scheme"
  ],
  "dynamicFilteringString": "behind-the-scene * * noop\nbehind-the-scene * inline-script noop\nbehind-the-scene * 1p-script noop\nbehind-the-scene * 3p-script noop\nbehind-the-scene * 3p-frame noop\nbehind-the-scene * image noop\nbehind-the-scene * 3p noop",
  "urlFilteringString": "",
  "hostnameSwitchesString": "no-large-media: behind-the-scene false\nno-csp-reports: * true",
  "userFilters": "! 2022-05-11 https://www.dumpert.nl\nwww.dumpert.nl##.didomi-popup__backdrop.didomi-notice-popup.didomi-popup-backdrop\n"
}