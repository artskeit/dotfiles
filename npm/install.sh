#!/bin/sh
#
# Install global NPM packages
#

if test ! $(which node)
then
  printf "${INFO}› Installing Node through nvm...${COLOR_RESET}\n"
  nvm install --lts
  printf "${SUCCESS}› Node installed${COLOR_RESET}\n"
fi

if test $(which npm);
then
  printf "${INFO}› Installing global NPM packages...${COLOR_RESET}\n"
  npm install -g npm-check-updates;
  printf "${SUCCESS}› Global NPM packages installed${COLOR_RESET}\n"
fi
