#!/bin/bash  
# Install SpotX (https://github.com/SpotX-Official/SpotX-Bash)

killall Spotify
curl -fsSL https://spotx-official.github.io/run.sh | bash -s -- -c $@
