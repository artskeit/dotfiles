alias reload!='. ~/.zshrc'

alias cl='clear' # Good 'ol Clear Screen command
alias cls='cl' # Good 'ol Clear Screen command

alias home='cd ~/'
alias sites='cd ~/Sites/'
alias dotfiles='cd ~/.dotfiles'

alias spotx='. ~/.dotfiles/spotify/install.sh'


alias vhosts='sudo vim /etc/apache2/extra/httpd-vhosts.conf'
alias serve='python3 -m http.server'
alias myip='echo "External: $(curl -s http://ipecho.net/plain) \nInternal: $(ipconfig getifaddr en0)"'

# NPM
alias npm-update='ncu -u'
