#!/bin/sh
#
# Antigen, the zsh package manager
#

if ! cat /etc/shells | grep $HOMEBREW_PREFIX/bin/zsh > /dev/null;
then
    title "Adding Homebrew Zsh to list of allowed shells."
    sudo sh -c 'echo ${HOMEBREW_PREFIX}/bin/zsh >> /etc/shells'
    echo
fi

# get newest antigen version
# curl -L git.io/antigen > antigen.zsh
